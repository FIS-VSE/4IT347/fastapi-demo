from fastapi import FastAPI
import spacy

app = FastAPI()
nlp = spacy.load("en_core_web_sm")


@app.get("/ner/{sentence}")
async def say_hello(sentence: str):
    doc = nlp(sentence)
    return {"message": str(doc.ents)}
