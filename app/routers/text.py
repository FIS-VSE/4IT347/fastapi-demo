import os

import spacy
from fastapi import APIRouter, status, HTTPException

from app.models.text import TextRequest, EntsResponse

model = "en_core_web_sm"
spacy.cli.download(model)
nlp = spacy.load(model)
token = os.getenv("OUR_TOKEN")

router = APIRouter(
    prefix="/api/v1/text",
    tags=["texts"],
    responses={404: {"description": "Not found"}}
)


@router.post("/ner/", response_model=EntsResponse)
async def ner(text_request: TextRequest):
    if text_request.apiKey != token:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Incorrect token",
        )
    doc = nlp(text_request.text)
    ents = [{'text': entity.text, 'label': entity.label_} for entity in doc.ents]
    return EntsResponse(entities=ents)
