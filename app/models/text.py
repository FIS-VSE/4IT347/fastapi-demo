from pydantic import BaseModel, Field
from typing_extensions import Annotated


class EntsResponse(BaseModel):
    entities: list


class TextRequest(BaseModel):
    text: Annotated[str, Field(..., min_length=10, max_length=500)]
    apiKey: str
