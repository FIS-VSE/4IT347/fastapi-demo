from fastapi import FastAPI, HTTPException, status
from app.routers import text

app = FastAPI()
app.include_router(text.router)


@app.get("/")
async def root():
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN
    )
